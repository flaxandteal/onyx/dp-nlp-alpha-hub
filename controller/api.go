package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab/flaxandteal/Onyx/dp-nlp-alpha-hub/clients"
	"io/ioutil"

	"github.com/ONSdigital/log.go/v2/log"
)

func MakeRequest(ctx context.Context, url string, params interface{}, resp interface{}) {
	cl := clients.New(url, params)

	log.Info(ctx, fmt.Sprintf("Making a request to: %s with query: %s", url, params))

	res, err := cl.DoRequest(ctx)
	if err != nil {
		log.Fatal(ctx, "", err)
	}
	defer res.Body.Close()

	log.Info(ctx, "request successful")

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(ctx, "issue parsing response body ", err)
	}

	if err := json.Unmarshal(b, &resp); err != nil {
		log.Fatal(ctx, "issue found unmarshaling resp to the given interface", err)
	}

}
