package params

import "net/url"

type CategoryParams struct {
	Query string `url:"query,omitempty"`
}

type BerlinParams struct {
	Query string `url:"q,omitempty"`
	State string `url:"state,omitempty"`
}

type ScrubberParams struct {
	Query string `url:"q,omitempty"`
}

type Filters struct {
	State string `url:"state,omitempty"`
}

func GetScrubberParams(query url.Values) *ScrubberParams {
	result := ScrubberParams{
		Query: "",
	}

	if len(query["q"]) == 1 {
		result.Query = query["q"][0]
	}

	return &result
}

func GetFilters(query url.Values) *Filters {
	result := Filters{
		State: "",
	}

	if len(query["state"]) == 1 {
		result.State = query["state"][0]
	}

	return &result
}

func GetBerlinParams(q string, fl Filters) *BerlinParams {
	return &BerlinParams{
		Query: q,
		State: fl.State,
	}
}

func GetCategoryParams(p string) *CategoryParams {
	return &CategoryParams{
		Query: p,
	}
}
