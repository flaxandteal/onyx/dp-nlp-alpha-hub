module gitlab/flaxandteal/Onyx/dp-nlp-alpha-hub

go 1.17

require (
	github.com/dghubble/sling v1.4.0
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/kelseyhightower/envconfig v1.4.0
)

require (
	github.com/ONSdigital/dp-api-clients-go v1.43.0 // indirect
	github.com/ONSdigital/dp-api-clients-go/v2 v2.92.2 // indirect
	github.com/ONSdigital/dp-net v1.4.1 // indirect
	github.com/ONSdigital/dp-net/v2 v2.0.0 // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/hokaccha/go-prettyjson v0.0.0-20211117102719-0474bc63780f // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20220204135822-1c1b9b1eba6a // indirect
)

require (
	github.com/ONSdigital/log.go v1.1.0 // indirect
	github.com/ONSdigital/log.go/v2 v2.2.0
	github.com/a-h/generate v0.0.0-20220105161013-96c14dfdfb60 // indirect
	github.com/atombender/go-jsonschema v0.9.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mitchellh/go-wordwrap v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sanity-io/litter v1.1.0 // indirect
	github.com/spf13/cobra v0.0.3 // indirect
	github.com/spf13/pflag v1.0.2 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
